package in.datami.catchup.client.domain.model;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Setter
@Getter
@AllArgsConstructor @NoArgsConstructor
@Builder
@ToString
@DynamoDBTable(tableName = "ltv-catchup-catalogue")
@Component
public class Catalogue {


  @Id
  @DynamoDBIgnore
  private PrimaryKey primaryKey;

  @DynamoDBAttribute(attributeName = "bitrate")
  private long bitrate;
  @DynamoDBAttribute(attributeName = "cpid")
  private String cpId;
  @DynamoDBAttribute(attributeName = "channelid")
  private String channelId;
  @DynamoDBAttribute(attributeName = "filename")
  private String fileName;
  @DynamoDBAttribute(attributeName = "fileloc")
  private String fileLoc;
  @DynamoDBAttribute(attributeName = "cloudfront")
  private String cloudFront;

  @DynamoDBAttribute(attributeName = "filetype")
  private String fileType;
  @DynamoDBIgnore
  private String tsRegex;


  @DynamoDBHashKey(attributeName = "ckey")
  public String getCatalogueKey() {
    return primaryKey != null ? primaryKey.getCatalogueKey() : null;
  }

  public void setCatalogueKey(final String partitionKey) {
    if (primaryKey == null)
      primaryKey = new PrimaryKey();
    primaryKey.setCatalogueKey(partitionKey);
  }

  @DynamoDBRangeKey(attributeName = "createdts")
  public long getCreatedTimeStamp() {
    return primaryKey != null ? primaryKey.getCreatedTimeStamp() : 0;
  }

  public void setCreatedTimeStamp(final long sortKey) {
    if (primaryKey == null) {
      primaryKey = new PrimaryKey();
    }
    primaryKey.setCreatedTimeStamp(sortKey);
  }


  @DynamoDBIgnore
  @JsonIgnore
  public Catalogue setTimeStampFromFileName (String fileName){
      Matcher matcher = Pattern.compile(this.tsRegex).matcher(fileName);
      if(matcher.matches()){
        this.getPrimaryKey().setCreatedTimeStamp(Long.parseLong(matcher.group(1)));
      }
      return this;
  }

}
