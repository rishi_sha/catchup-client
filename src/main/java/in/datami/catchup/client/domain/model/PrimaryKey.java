package in.datami.catchup.client.domain.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@DynamoDBDocument
public class PrimaryKey {
  @DynamoDBHashKey(attributeName = "ckey")
  private String catalogueKey;

  @DynamoDBRangeKey(attributeName = "createdts")
  private long createdTimeStamp;
}
