package in.datami.catchup.client.repositories;


import in.datami.catchup.client.domain.model.Catalogue;
import in.datami.catchup.client.domain.model.PrimaryKey;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@EnableScan
@Repository
public interface CatalogueRepository extends CrudRepository<Catalogue, PrimaryKey> {

}
