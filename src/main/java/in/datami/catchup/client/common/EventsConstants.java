package in.datami.catchup.client.common;

public class EventsConstants {
  public static final String EVENT_READ_MASTER_M3U8 = "reader.master.m3u8";
  public static final String EVENT_START_POLLING = "polling";
  public static final String EVENT_STORE_FILES= "store.files";
  public static final String EVENT_ADD_CATALOGUE= "add.catalogue";
}
