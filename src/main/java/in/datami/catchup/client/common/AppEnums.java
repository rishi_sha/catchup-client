package in.datami.catchup.client.common;

public class AppEnums {
  public enum FileType {
    TS("ts"),M3U8("m3u8");

    private String name;
    FileType (String name) {  this.name = name; }
    public String getName () { return name; }
  }
}
