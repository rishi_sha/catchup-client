package in.datami.catchup.client.common;

public class BeanNameConstants {

  public static final String AWS_REGION = "AWS_REGION";
  public static final String AWS_SECRET_KEY = "AWS_SECRET_KEY";
  public static final String AWS_ACCESS_KEY = "AWS_ACCESS_KEY";
  public static final String AWS_ENDPOINT_URL = "AWS_ENDPOINT_URL";
  public static final String AWS_CATCHUP_BUCKET = "AWS_CATCHUP_BUCKET";

  public static final String TS_FILE_LOC = "TS_FILE_LOC";
  public static final String CATCH_CLIENT_VERTICLE = "CATCH_CLIENT_VERTICLE";
  public static final String POLLING_VERTICLE = "POLLING_VERTICLE";
  public static final String DATASTORE_VERTICLE = "DATASTORE_VERTICLE";
  public static final String CATALOGUE_VERTICLE = "CATALOGUE_VERTICLE";
}
