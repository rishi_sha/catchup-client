package in.datami.catchup.client.verticles;

import in.datami.catchup.client.common.AppEnums;
import in.datami.catchup.client.common.EventsConstants;
import in.datami.catchup.client.domain.model.Catalogue;
import in.datami.catchup.client.domain.model.PrimaryKey;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;


@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PollingVerticle extends AbstractVerticle {

  @Override
  public void start() {
    vertx.eventBus().consumer(EventsConstants.EVENT_START_POLLING,
                              (Message<JsonObject> tMessage) -> {
                                System.out.println("Polling : received message "+ tMessage.body());
                                vertx.executeBlocking(call -> downloadM3u8FileAndStartPolling(tMessage));
                                scheduleNextUpdate(tMessage);
                              });

  }

  private void downloadM3u8FileAndStartPolling (Message<JsonObject> tMessage) {
    Catalogue catalogue = tMessage.body().mapTo(Catalogue.class);
    RestTemplate restTemplate = new RestTemplate();
    try{
        byte[] m3U8Bytes = restTemplate.getForObject(catalogue.getCloudFront() + catalogue.getFileLoc(), byte[].class);
        readM3U8AndSendForPolling(vertx, m3U8Bytes, catalogue);
    }catch (Exception ex){
      System.err.println("error retrieving ts file" + catalogue.getFileLoc());
      ex.printStackTrace();
    }
  }

  private void readM3U8AndSendForPolling (Vertx vertx,
                                          byte[] resp,
                                          Catalogue cp) {
    try {
      System.out.println("Segment reading response");
      Path segmentFile = Files.createTempFile(cp.getFileName(), null);
      Files.write(segmentFile, resp);
      List<JsonObject> catalogueList =  Files.readAllLines(segmentFile)
                                            .stream()
                                            .filter(line -> !line.startsWith("#") && StringUtils.isNotBlank(line))
                                            .map(line -> {
             System.out.println("Segment M3u8 line: "+ line);
             String fileName = line.substring(line.lastIndexOf("/")+1);

             return JsonObject.mapFrom(Catalogue.builder().cpId(cp.getCpId())
                                                .bitrate(cp.getBitrate())
                                                .tsRegex(cp.getTsRegex())
                                                .channelId(cp.getChannelId())
                                                .fileName(fileName)
                                                .fileType(AppEnums.FileType.TS.getName())
                                                .cloudFront(cp.getCloudFront())
                                                .fileLoc(cp.getFileLoc().substring(0, cp.getFileLoc().lastIndexOf("/")+1) + line)
                                                .primaryKey(new PrimaryKey(cp.getChannelId() + "_" + fileName + "_" + cp.getBitrate(), cp.getCreatedTimeStamp()))
                                                .build()
                                                .setTimeStampFromFileName(fileName));
           }).collect(Collectors.toList());
      vertx.eventBus()
           .send(EventsConstants.EVENT_STORE_FILES, new JsonArray(catalogueList));

      Files.delete(segmentFile);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }


  private void scheduleNextUpdate (Message<JsonObject> tMessage) {
    vertx.setPeriodic(10000, aLong -> {
      System.out.println(" new timer "+ aLong);
      System.out.println("sending message " + tMessage.body().toString());
      vertx.eventBus()
           .send(EventsConstants.EVENT_START_POLLING,
                 tMessage.body());
    });
  }
}
