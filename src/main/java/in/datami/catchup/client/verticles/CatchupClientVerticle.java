package in.datami.catchup.client.verticles;

import com.fasterxml.jackson.core.type.TypeReference;
import in.datami.catchup.client.SpringVerticleFactory;
import in.datami.catchup.client.common.AppEnums;
import in.datami.catchup.client.common.EventsConstants;
import in.datami.catchup.client.config.ApplicationConfig;
import in.datami.catchup.client.config.CpCfDistConfig;
import in.datami.catchup.client.domain.model.Catalogue;
import in.datami.catchup.client.domain.model.PrimaryKey;
import io.vertx.core.*;
import io.vertx.core.json.JsonObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.*;
import java.util.Arrays;
import java.util.List;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CatchupClientVerticle extends AbstractVerticle {

  @Autowired
  ApplicationContext context;

  @Autowired
  ApplicationConfig config;

  @Autowired
  SpringVerticleFactory verticleFactory;

  @Override
  public void init (Vertx vertx, Context context) {
    super.init(vertx, context);
    DeploymentOptions options = new DeploymentOptions().setWorker(true).setInstances(1);
    vertx.deployVerticle(getVerticle(DataStoreVerticle.class), options.setInstances(2),
                         getDeploymentHandler("dataStoreVerticle"));
    vertx.deployVerticle(getVerticle(PollingVerticle.class), options,
                         getDeploymentHandler("pollingVerticle"));
  }

  @Override
  public void start () throws Exception {
    String configFileName = config.getFileStoreLoc() + File.separatorChar + "cp-channel-config.json";
    List<CpCfDistConfig> cpCfDistConfigMap =config.mapper().readValue(
        Files.newInputStream(FileSystems.getDefault().getPath(configFileName)),
                             new TypeReference<List<CpCfDistConfig>>() {});

    RestTemplate restTemplate = new RestTemplate();
    cpCfDistConfigMap.forEach(cp ->
      vertx.executeBlocking(call -> {
        try{
          restTemplate.execute(cp.getCloudFront() + cp.getM3u8Location(), HttpMethod.GET,
                               request -> request.getHeaders().setAccept(
                                 Arrays.asList(MediaType.APPLICATION_OCTET_STREAM, MediaType.ALL)),
                               response -> {
                                 readM3U8AndSendForPolling(vertx, response.getBody(), cp);
                                 return null;
                               });
        }catch (Exception ex){
          System.err.println("error retrieving ts file " + cp.getM3u8Location());
          ex.printStackTrace();

          config.getAwsConfig().transferManager().shutdownNow(true);
          config.getAwsConfig().amazonS3().shutdown();
          config.getAwsConfig().amazonDynamoDB().shutdown();
          vertx.close();
          System.gc();
          System.exit(-500);
        }
      }));

  }

  private void readM3U8AndSendForPolling (Vertx vertx,
                                          InputStream resp,
                                          CpCfDistConfig cp) {
    try {
      System.out.println("Master Reading response ");
      Path directory =
        Files.createDirectories(Paths.get(config.getFileStoreLoc()+ File.separatorChar + cp.getChannelId()));
      Files.deleteIfExists(Paths.get(directory.toString(), "master.m3u8"));
      Path masterFile = Files.createFile(Paths.get(directory.toString(), "master.m3u8"));
      Files.copy(resp, masterFile, StandardCopyOption.REPLACE_EXISTING);
      List<String> lines = Files.readAllLines(masterFile);
      for(int i=2; i< lines.size();i++){
        if(!lines.get(i).contains(":") || StringUtils.isBlank(lines.get(i)))
          continue;
        String line1 = lines.get(i++);
        String line2 = lines.get(i);

        System.out.println("Master M3u8 line 1: "+ line1);
        System.out.println("Master M3u8 line 2: "+ line2);
        long bitrate = Long.parseLong(line1.split(":")[1].split(",")[0].split("=")[1]);
        String fileName = line2.contains("/")? line2.substring(line2.lastIndexOf("/")):line2;
        Catalogue catalogue = Catalogue.builder()
                 .bitrate(bitrate)
                 .channelId(cp.getChannelId())
                 .cloudFront(cp.getCloudFront())
                 .cpId(cp.getCp())
                 .fileType(AppEnums.FileType.M3U8.getName())
                 .tsRegex(cp.getTsFileNameRegex())
                 .fileName(fileName)
                 .fileLoc(cp.getM3u8Location().substring(0, cp.getM3u8Location().lastIndexOf("/"))+ "/" +line2)
                 .primaryKey(new PrimaryKey(fileName, System.currentTimeMillis() ))
                 .build();

        vertx.eventBus()
             .send(EventsConstants.EVENT_START_POLLING,
                   JsonObject.mapFrom(catalogue));
      }
      Files.readAllLines(masterFile)
           .stream()
           .filter(line -> !line.startsWith("#") && StringUtils.isNotBlank(line))
           .forEach(line -> {

           });
      Files.delete(masterFile);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private <T> String getVerticle (Class<T> verticleClass) {
    return verticleFactory.prefix() + ":" + context.getBean(verticleClass).getClass().getName();
  }


  private Handler<AsyncResult<String>> getDeploymentHandler (String verticleName) {
    return res -> {
      if (res.succeeded()) {
        System.out.println(verticleName + " deployed");
      } else {
        System.out.println(verticleName + " deploymentFailed");
        System.out.println(res.result());
        res.cause().printStackTrace();
      }
    };
  }

}
