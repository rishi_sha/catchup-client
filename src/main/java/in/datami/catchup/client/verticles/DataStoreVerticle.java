package in.datami.catchup.client.verticles;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressEventType;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;
import in.datami.catchup.client.common.BeanNameConstants;
import in.datami.catchup.client.common.EventsConstants;
import in.datami.catchup.client.domain.model.Catalogue;
import in.datami.catchup.client.repositories.CatalogueRepository;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DataStoreVerticle extends AbstractVerticle {

  @Autowired
  TransferManager transferManager;

  @Autowired
  CatalogueRepository catalogueRepository;

  @Autowired
  @Qualifier(BeanNameConstants.AWS_CATCHUP_BUCKET)
  String awsS3CatchupBucket;

  @Override
  public void start() {
    EventBus bus = vertx.eventBus();
    bus.consumer(EventsConstants.EVENT_STORE_FILES, this::downloadAndUploadToStorage);
  }


  private void downloadAndUploadToStorage (Message<JsonArray> tMessage) {

    System.out.println("DataStore : received message "+ tMessage.body());
    vertx.executeBlocking(call -> {
      RestTemplate restTemplate = new RestTemplate();
      tMessage.body().stream().forEach(json -> {
        Catalogue catalogue = ((JsonObject)json).mapTo(Catalogue.class);
        System.out.println(" Requesting for file :: "+ catalogue.getFileLoc());

        try{
          byte[] m3U8Bytes = restTemplate.getForObject(catalogue.getCloudFront()+"/" + catalogue.getFileLoc(),
                                                       byte[].class);
          Optional<File> dFile =  downloadTsFiles(m3U8Bytes, catalogue);
          dFile.ifPresent(file -> uploadToStorage(catalogue, file));
        }catch (Exception ex){
          System.err.println("error retrieving ts file" + catalogue.getFileLoc());
          ex.printStackTrace();
        }
      });
    });

  }

  private Optional<File> downloadTsFiles (byte[] resp,
                                         Catalogue cp) {
    try {
      String fileName = cp.getFileName();
      System.out.println("Filename: "+ fileName);
      String[] fileNameArr = fileName.split("\\.");
      fileName = fileNameArr[0]+ "_" + cp.getBitrate() + "." + fileNameArr[1];
      cp.setFileName(fileName);
      Path segmentFile = Files.createTempFile(fileName, null);
      Files.write(segmentFile, resp);
      return Optional.of(segmentFile.toFile());
    } catch (IOException e) {
      e.printStackTrace();
    }
    return Optional.empty();
  }


  private void uploadToStorage (Catalogue catalogue, File tsFile){

    try {
      String awsFileName = catalogue.getCpId()+File.separatorChar +
                          catalogue.getChannelId()+ File.separatorChar +
                          "chunks"+ File.separatorChar + catalogue.getFileName();
      Upload upload =  transferManager.upload(awsS3CatchupBucket, awsFileName, tsFile);
      upload.addProgressListener(new ProgressListener() {
        // This method is called periodically as your transfer progresses
        public void progressChanged (ProgressEvent progressEvent) {
          System.out.println(upload.getProgress().getPercentTransferred() + "%");

          if (progressEvent.getEventType() == ProgressEventType.TRANSFER_COMPLETED_EVENT) {
            System.out.println("file uploaded successfully." + catalogue.getFileLoc());
            addToDb(catalogue);
            if(tsFile.delete())
              System.out.println(" File Deleted successfully");
          }
        }});


    } catch (AmazonServiceException ex) {
      System.err.println("error [" + ex.getMessage() + "] occurred while uploading [" + catalogue.getFileLoc() + "] ");
    }
  }

  private void addToDb(Catalogue body){
    System.out.println("Adding to Db: "+ body.toString());
    try {
      body.setFileLoc(body.getCpId()+ "/" +body.getChannelId() + "/chunks/" + body.getFileName());
      catalogueRepository.save(body);
      System.out.println("Successfully saved to Db");
    }catch(Exception ex){
      System.out.println("Failure in saving to Db");
      ex.printStackTrace();
    }
  }

  @Override
  public void stop ()  {
    transferManager.shutdownNow(true);
  }
}
