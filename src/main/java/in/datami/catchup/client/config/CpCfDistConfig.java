package in.datami.catchup.client.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CpCfDistConfig {
  private String cp;
  private String channelId;
  private String cloudFront;
  private String m3u8Location;
  private int streamDiffDuration;
  private String tsFileNameRegex;
  private Map<String, Object> additionalProperties;
}
