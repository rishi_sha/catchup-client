package in.datami.catchup.client.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import in.datami.catchup.client.common.BeanNameConstants;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.dns.AddressResolverOptions;
import io.vertx.micrometer.MicrometerMetricsOptions;
import io.vertx.micrometer.VertxJmxMetricsOptions;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Getter
@Setter
@Configuration
@EnableAspectJAutoProxy
@ConfigurationProperties(prefix = "application")
public class ApplicationConfig {


  String fileStoreLoc;

  ObjectMapper mapper = new ObjectMapper();

  @Autowired
  AwsConfig awsConfig;

  @Bean
  public Vertx vertx(){
    return Vertx.vertx(new VertxOptions()
                         .setMetricsOptions(
                           new MicrometerMetricsOptions()
                             .setJmxMetricsOptions(new VertxJmxMetricsOptions().setEnabled(true)
                                                  .setStep(5)
                                                  .setDomain("catchup.client"))
                             .setEnabled(true)));
  }

  @Bean(name = BeanNameConstants.TS_FILE_LOC)
  public String getFileStoreLoc () {
    return fileStoreLoc;
  }

  public ObjectMapper mapper(){ return this.mapper; }
}
