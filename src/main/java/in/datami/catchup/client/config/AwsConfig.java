package in.datami.catchup.client.config;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import in.datami.catchup.client.common.BeanNameConstants;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@AllArgsConstructor @NoArgsConstructor
@Configuration
@EnableDynamoDBRepositories
  (basePackages = "in.datami.catchup.client.repositories")
public class AwsConfig {
  @Value("${amazon.dynamodb.endpoint}")
  @Qualifier(BeanNameConstants.AWS_ENDPOINT_URL)
  private String amazonDynamoDBEndpoint;

  @Value("${amazon.aws.accesskey}")
  @Qualifier(BeanNameConstants.AWS_ACCESS_KEY)
  private String amazonAWSAccessKey;

  @Value("${amazon.aws.secretkey}")
  @Qualifier(BeanNameConstants.AWS_SECRET_KEY)
  private String amazonAWSSecretKey;

  @Value("${amazon.aws.region}")
  private String amazonAwsRegion;

  @Value("${amazon.aws.catchup-bucket}")
  private String amazonAwsCatchupBucket;

  @Bean
  public AWSCredentials amazonAWSCredentials () {
    return new BasicAWSCredentials(
      getAmazonAWSAccessKey(), getAmazonAWSSecretKey());
  }

  public AWSCredentialsProvider amazonAWSCredentialsProvider() {
    return new AWSStaticCredentialsProvider(amazonAWSCredentials());
  }

  public AwsClientBuilder.EndpointConfiguration endpointConfiguration () {
    return new AwsClientBuilder.EndpointConfiguration(getAmazonDynamoDBEndpoint(), getAmazonAwsRegion());
  }

  @Bean
  public AmazonDynamoDB amazonDynamoDB() {
    return AmazonDynamoDBClientBuilder.standard()
                                      .withCredentials(amazonAWSCredentialsProvider())
                                      .withRegion(getAmazonAwsRegion()).build();
  }

  @Bean
  public AmazonS3 amazonS3(){
    return AmazonS3ClientBuilder.standard()
                                .withCredentials(amazonAWSCredentialsProvider())
                                .withRegion(getAmazonAwsRegion()).build();
  }

  @Bean
  public TransferManager transferManager(){
    return TransferManagerBuilder.standard()
                                               .withS3Client(amazonS3())
                                               .build();
  }

  @Bean(name = BeanNameConstants.AWS_CATCHUP_BUCKET)
  public String getCatchupBucket () {
    return this.amazonAwsCatchupBucket;
  }
}
