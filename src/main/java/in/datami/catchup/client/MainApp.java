package in.datami.catchup.client;

import in.datami.catchup.client.verticles.CatchupClientVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.EventListener;

@SpringBootApplication
@ComponentScan("in.datami.catchup.client")
public class MainApp {


  @Autowired
  Vertx vertx;

  @Autowired
  SpringVerticleFactory verticleFactory;

  private static final Logger LOG = LoggerFactory
    .getLogger(MainApp.class);

  public static void main(String[] args)  {
    LOG.info("STARTING THE APPLICATION");
    SpringApplication.run(MainApp.class, args);

    LOG.info("APPLICATION FINISHED");
  }

  @EventListener(ApplicationReadyEvent.class)
  public void deployVerticle() {
    if(null == verticleFactory)
      System.err.println("null verticle factory");
    vertx.registerVerticleFactory(verticleFactory);

    DeploymentOptions options = new DeploymentOptions().setInstances(1);
    vertx.deployVerticle(verticleFactory.prefix() + ":" + CatchupClientVerticle.class.getName(), options,res -> {
      if (res.succeeded()) {
        System.out.println("clientVerticle deployed");
      } else {
        System.out.println("clientVerticle Deployment failed!");

        System.out.println(res.result());
        res.cause().printStackTrace();
      }
    });
  }

}
